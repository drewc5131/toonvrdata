import hashlib
import os
jsonString = '{\n"data": ['
rootDir = 'P:\Epic Games\Projects\ToonVR\BUILD'
for dirName, subdirList, fileList in os.walk(rootDir):
    for fname in fileList:
        if not dirName.startswith("P:\\Epic Games\\Projects\\ToonVR\\BUILD\\WindowsNoEditor\\ToonVR\\Saved") and not dirName.startswith("P:\\Epic Games\\Projects\\ToonVR\\BUILD\\.git"):
            filename = "%s\\%s"%(dirName.replace("P:\\Epic Games\\Projects\\ToonVR\\BUILD\\WindowsNoEditor\\" ,""), fname)
            if filename not in ["P:\\Epic Games\\Projects\\ToonVR\\BUILD\\.gitignore", "P:\\Epic Games\\Projects\\ToonVR\\BUILD\\lotomanifest.json", "P:\\Epic Games\\Projects\\ToonVR\\BUILD\\makemanifest.py",
                        "P:\\Epic Games\\Projects\\ToonVR\\BUILD\\launcher.json"]:
                jsonString = jsonString + '{"Filename": "%s", "sha256" : "%s"},\n' %(filename, os.stat("%s" %"%s\\%s" %(dirName, fname)).st_size)
    
jsonString = jsonString[:-2] + '\n]}'
jsonString = jsonString.replace("\\", "/").replace("P:/Epic Games/Projects/ToonVR/BUILD/WindowsNoEditor/", "")
print(jsonString)
file = open("lotomanifest.json", "w")
file.write(jsonString)